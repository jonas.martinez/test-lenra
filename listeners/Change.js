module.exports = function change(action, data, event) {
    switch (action) {
        case "ChangeData":
            data.data = event.value;
            break;
        case "ChangeFunction":
            data.function = event.value;
            break;
    }
    return data;
}
